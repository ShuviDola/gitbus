from Bus import *
from Billete import *
def menu():
	print("1.- Comprar Billete\n2.- Vender Billete\n3.- Ver Estados del billete\n0.-Salir\n")

def menuBus():
	print("      1.- ALSA\n      2.- Bacoma\n      3.- Autobuses Pepe\n")


def validarDNI(dni):
	tabla = "TRWAGMYFPDXBNJZSQVHLCKE"
	dig_ect = "XYZ"
	reemp_dig_ect = {'X':'0','Y':'1','Z':'2'}
	numeros="1234567890"
	dni = dni.upper()
	if(len(dni) == 9):
		dig_control = dni[8]
		dni = dni[:8]
		if dni[0] in dig_ect:
			dni = dni.replace(dni[0], reemp_dig_ect[dni[0]])
		return len(dni) == len ([n for n in dni if n in numeros]) and tabla[int(dni)%23] == dig_control
	return False	

bus1 = Bus(60, 0, "ALSA", 1)
bus2 = Bus(20, 0, "Bacoma", 2)
bus3 = Bus(35, 0, "AutocaresPepe", 3)

opcion = 'q'

while(opcion != '0'):

	menu()
	opcion = input("Selecciona una opcion: ")

	if(opcion == '1'):
		try:
			dni = input("Dime tu dni: ")
			nombre= input("Dime tu nombre: ")
			billete = Billete(nombre, dni)
			menuBus()
			busSelected = input("Selecciona el bus del billete que desea comprar: ")

			print("El dni es: ", validarDNI(dni))

			if(busSelected == '1'):
				print(ventaBilletes(billete))
			elif (busSelected == '2'):
				print(ventaBilletes(billete))
			elif (busSelected == '3'):
				print(ventaBilletes(billete))
			else:
				print ("ERROR, no has introducido ningun bus")	
			

		except ValueError:
			print ("ERROR, Nombre erroneo")
		
	if(opcion == '2'):
		nombre = input("Dime tu nombre: ")
		dni = input("Dime tu dni: ")

		menuBus()
		busSelected = input("A que bus pertenece su billete?: ")
		billete = Billete(nombre, dni)

		if(busSelected == '1'):
			print(devolucionBillete(billete))
		elif (busSelected == '2'):
			print(devolucionBillete(billete))
		elif (busSelected == '3'):
			print(devolucionBillete(billete))
		else:
			print ("ERROR, no has introducido ningun bus")	

	if(opcion == '3'):

		busSelected = input("De que bus quieres saber el tamaño?")
		if(busSelected == '1'):
			print(bus1.__str__())
		elif (busSelected == '2'):
			print(bus2.__str__())
		elif (busSelected == '3'):
			print(bus3.__str__())
		else:
			print ("ERROR, no has introducido ningun bus")
		
	
print("Saliendo....")
