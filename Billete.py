class Billete():
	def __init__(self, nombre, dni):
		self.__nombre = nombre
		self.__dni = dni

	def getNombre(self):
		return self.__nombre

	def getDni(self):
		return self.__dni

	def __str__(self):
		return "Este billete esta a nombre de "+self.__nombre+" con DNI "+self.__dni + "\n"