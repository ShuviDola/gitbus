class Bus:
    def __init__(self,capacidadTotal,asientosOcupados,marca,id):
        self.__CapacidadTotal = capacidadTotal
        self.__asientosOcupados = asientosOcupados
        self.__marca = marca
        self.__id = id
        self.__listaBilletes = []*capacidadTotal

    def getCapacidadTotal(self):
        return self.__CapacidadTotal

    def setAsientosOcupados(self,asientosOcupados):
        if type(asientosOcupados) is int:
            self.__asientosOcupados = asientosOcupados
        else:
            raise ValueError

    def getAsientosOcupados(self):
        return self.__asientosOcupados

    def getMarca(self):
        return self.__marca

    def ventaBilletes(self,billete):

        for billeteLista in self.__listaBilletes:
            if billeteLista.getDni() == billete.getDni():
                return "Ya has comprado un billete"
                
        self.__listaBilletes.append(billete)
        self.__asientosOcupados = self.__asientosOcupados +1
        return "Billete comprado"

    def devolucionBillete(self,billete):
        for billeteLista in self.__listaBilletes:
            if billeteLista.getDni() == billete.getDni():
                self.__listaBilletes.remove(billeteLista)
                self.__asientosOcupados = self.__asientosOcupados -1
                return "El billete ha sido devuelto"

        return "El billete no existe"

    def __str__(self):
        lista = ""
        for x in self.__listaBilletes: lista += x.__str__()
        lista += "\n"
        salida = "La capacidad total de " + self.__marca + " es: " + str(self.__CapacidadTotal) + ", los asientos ocupados son: " + str(self.__asientosOcupados) +"\n       " + lista
        return salida



